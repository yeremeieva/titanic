import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df

def calculate_median(frame):
    return int(frame['Age'].median().round())


def count_missing_values(frame):
    return frame['Age'].isna().sum()


def name_mapping(name):
    if 'Mr.' in name:
        return 'Mr.'
    elif 'Miss.' in name:
        return 'Miss.'
    elif 'Mrs.' in name:
        return 'Mrs.'
    else:
        return name


def get_filled():
    frame = get_titatic_dataframe()

    mr = frame[frame['Name'].map(name_mapping) == 'Mr.']
    mrs = frame[frame['Name'].map(name_mapping) == 'Mrs.']
    miss = frame[frame['Name'].map(name_mapping) == 'Miss.']
    return [('Mr.', count_missing_values(mr), calculate_median(mr)), ('Mrs.', count_missing_values(mrs), calculate_median(mrs)),
            ('Miss.', count_missing_values(miss), calculate_median(miss))]